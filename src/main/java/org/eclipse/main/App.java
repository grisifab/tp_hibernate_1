package org.eclipse.main;

import java.util.List;

import org.eclipse.model.Personne;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;
import org.hibernate.criterion.Restrictions;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args )
    {
       Personne leGars = new Personne();
       leGars.setNom("de Mikonos");
       leGars.setPrenom("Nikos");
       
       System.out.println("jusqu'ici tout va bien 1");
       Configuration configuration = new Configuration().configure();
       System.out.println("jusqu'ici tout va bien 2");
       SessionFactory sessionFactory = configuration.buildSessionFactory();
       System.out.println("jusqu'ici tout va bien 3");
       Session session = sessionFactory.openSession();
       System.out.println("jusqu'ici tout va bien 4");
       Transaction transaction = session.beginTransaction();
       System.out.println("jusqu'ici tout va bien 5");
       session.persist(leGars);								// ecriture
       System.out.println("jusqu'ici tout va bien 6");
       Integer cle = (Integer) session.save(leGars); 
       
       Personne leGars2 = session.load(Personne.class, 2); // Lecture bdd
       System.out.println(leGars2);
       
       Personne leGars3 = session.get(Personne.class, 3); // Lecture bdd
       System.out.println(leGars3);
       
       Personne leGars4 = session.get(Personne.class, 6); // modif personne id 6
       leGars4.setNom("Lorenzo");
       session.flush();
       
       Personne leGars5 = session.get(Personne.class, cle); // suppression
       session.delete(leGars5);
       
       Criteria criteria = session.createCriteria(Personne.class); // affichage de la liste complète
       List<Personne> personness = (List<Personne>) criteria.list();
       for (Personne personne : personness)
    	   System.out.println(personne);
       
       String string = "Wick";										// affichage de la liste selon critère
       criteria = criteria.add(Restrictions.eq("nom", string));
       personness = (List<Personne>) criteria.list();
       for (Personne personne : personness)
    	   System.out.println(personne);
       
       Personne leGars6 = session.get(Personne.class, 7);
       leGars6.setNom("Taistoi");
       //session.refresh(leGars6);			// annule la modification
       System.out.println(leGars6);
       
       transaction.commit(); 
       session.close(); 
       sessionFactory.close();
       System.out.println("jusqu'ici tout va bien 7");
       System.out.println(cle); // = id de la dernière personne de la base de donnée
       
    }
}
